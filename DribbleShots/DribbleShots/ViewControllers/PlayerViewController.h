//
//  PlayerViewController.h
//  DribbleShots
//
//  Created by Rodrigo Kieffer on 8/30/15.
//  Copyright (c) 2015 Rodrigo Kieffer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Shot.h"

@interface PlayerViewController : UIViewController

@property (strong, nonatomic) Shot *shot;

@end
