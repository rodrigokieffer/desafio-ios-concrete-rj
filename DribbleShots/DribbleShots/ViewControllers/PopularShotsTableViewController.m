//
//  PopularShotsTableViewController.m
//  DribbleShots
//
//  Created by Rodrigo Kieffer on 8/30/15.
//  Copyright (c) 2015 Rodrigo Kieffer. All rights reserved.
//

#import "PopularShotsTableViewController.h"
#import <AFNetworking/AFNetworking.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "PlayerViewController.h"
#import "Constants.h"
#import "Shot.h"
#import "ShotCell.h"

@interface PopularShotsTableViewController ()

@property (strong, nonatomic) NSMutableArray *shots;
@property (assign, nonatomic) int lastPageVisited;
@property (assign, nonatomic) int totalPages;

@end

@implementation PopularShotsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self fetchShots];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.navigationItem.title = @"Popular Shots";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - fetch shots
- (void)fetchShots
{
    if (!_shots) {
        _shots = [NSMutableArray array];
    }
    
    _lastPageVisited++;
    
    NSString *urlString = [NSString stringWithFormat:@"http://api.dribbble.com/shots/popular?access_token=%@&page=%i",ClientToken, _lastPageVisited];
    
    NSURL *url = [[NSURL alloc] initWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *json = responseObject;
        _totalPages = [[json objectForKey:@"pages"] intValue];
        
        NSDictionary *shotsDic = [json objectForKey:@"shots"];        
        for (NSDictionary *dictionary in shotsDic) {
            Shot *shot = [MTLJSONAdapter modelOfClass:[Shot class] fromJSONDictionary:dictionary error:nil];
            [_shots addObject:shot];
        }
        
        [self.tableView reloadData];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"failed: %@",error.localizedDescription);
    }];
    
    [operation start];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_shots count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ShotCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ShotCellIdentifier" forIndexPath:indexPath];
    
    // Configure the cell...
    Shot *shot = [_shots objectAtIndex:indexPath.row];
    cell.labelTitle.text = shot.title;
    cell.labelViews.text = [NSString stringWithFormat:@"%ld", shot.viewsCount];
    
    if (shot.imageUrl != (id)[NSNull null]) {
        [cell.image sd_setImageWithURL:[NSURL URLWithString:shot.imageUrl] placeholderImage:[UIImage imageNamed:@"download-icon.png"]];
    }
    
    if ((_lastPageVisited < _totalPages) && (indexPath.row == _shots.count - 1)) {
        [self fetchShots];
    }

    return cell;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    ShotCell *cell = (ShotCell *)sender;
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    Shot *shot = [_shots objectAtIndex:indexPath.row];
    
    if ([[segue identifier] isEqualToString:SegueToPlayerDetailVC]) {
        PlayerViewController *playeViewController = segue.destinationViewController;
        playeViewController.shot = shot;
        self.navigationItem.title = @"";
    }
}

@end
