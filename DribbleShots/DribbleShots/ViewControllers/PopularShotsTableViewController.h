//
//  PopularShotsTableViewController.h
//  DribbleShots
//
//  Created by Rodrigo Kieffer on 8/30/15.
//  Copyright (c) 2015 Rodrigo Kieffer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PopularShotsTableViewController : UITableViewController

@end
