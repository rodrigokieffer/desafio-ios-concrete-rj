//
//  PlayerViewController.m
//  DribbleShots
//
//  Created by Rodrigo Kieffer on 8/30/15.
//  Copyright (c) 2015 Rodrigo Kieffer. All rights reserved.
//

#import "PlayerViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface PlayerViewController ()

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelViews;
@property (weak, nonatomic) IBOutlet UIImageView *imageShot;
@property (weak, nonatomic) IBOutlet UIImageView *imagePlayerAvatar;
@property (weak, nonatomic) IBOutlet UILabel *labelPlayerName;
@property (weak, nonatomic) IBOutlet UIWebView *webViewShotDescription;

@end

@implementation PlayerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _labelTitle.text = _shot.title;
    _labelViews.text = [NSString stringWithFormat:@"%ld", _shot.viewsCount];
    [_imageShot sd_setImageWithURL:[NSURL URLWithString:_shot.imageUrl] placeholderImage:[UIImage imageNamed:@"download-icon.png"]];
    [_imagePlayerAvatar sd_setImageWithURL:[NSURL URLWithString:_shot.player.avatarUrl] placeholderImage:[UIImage imageNamed:@"download-icon.png"]];
    _labelPlayerName.text = _shot.player.playerName;
    [_webViewShotDescription loadHTMLString:_shot.shotDescription baseURL:nil];
    [_webViewShotDescription stringByEvaluatingJavaScriptFromString: @"document.body.style.fontFamily = 'Helvetica-Neue'"];
    
    CGFloat radiusImage = _imagePlayerAvatar.frame.size.width / 2;
    _imagePlayerAvatar.layer.masksToBounds = YES;
    _imagePlayerAvatar.layer.cornerRadius = radiusImage;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
