//
//  Shot.h
//  DribbleShots
//
//  Created by Rodrigo Kieffer on 8/30/15.
//  Copyright (c) 2015 Rodrigo Kieffer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>
#import "Player.h"

@interface Shot : MTLModel <MTLJSONSerializing>

@property (strong, nonatomic) NSString *title;
@property (assign, nonatomic) long viewsCount;
@property (strong, nonatomic) NSString *imageUrl;
@property (strong, nonatomic) Player *player;
@property (strong, nonatomic) NSString *shotDescription;

@end
