//
//  Shot.m
//  DribbleShots
//
//  Created by Rodrigo Kieffer on 8/30/15.
//  Copyright (c) 2015 Rodrigo Kieffer. All rights reserved.
//

#import "Shot.h"

@implementation Shot

+ (NSDictionary*)JSONKeyPathsByPropertyKey {
    
    return @{
               @"title": @"title",
               @"viewsCount": @"views_count",
               @"imageUrl": @"image_url",
               @"player": @"player",
               @"shotDescription": @"description"
            };
}

+ (NSValueTransformer *)playerJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[Player class]];
}

@end
