//
//  Player.h
//  DribbleShots
//
//  Created by Rodrigo Kieffer on 8/30/15.
//  Copyright (c) 2015 Rodrigo Kieffer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>

@interface Player : MTLModel<MTLJSONSerializing>

@property (strong, nonatomic) NSString *playerName;
@property (strong, nonatomic) NSString *avatarUrl;

@end
