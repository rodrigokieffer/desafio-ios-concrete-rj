//
//  Player.m
//  DribbleShots
//
//  Created by Rodrigo Kieffer on 8/30/15.
//  Copyright (c) 2015 Rodrigo Kieffer. All rights reserved.
//

#import "Player.h"

@implementation Player

+ (NSDictionary*)JSONKeyPathsByPropertyKey {
    return @{
             @"playerName": @"name",
             @"avatarUrl": @"avatar_url"
             };
}


@end
