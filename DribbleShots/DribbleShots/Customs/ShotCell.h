//
//  ShotCell.h
//  DribbleShots
//
//  Created by Rodrigo Kieffer on 8/30/15.
//  Copyright (c) 2015 Rodrigo Kieffer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShotCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *labelViews;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;

@end
